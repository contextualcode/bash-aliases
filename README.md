# Conextual Code bash aliases
Commonly used bash aliases

## Installation
1. Clone this repository:
    ```
    git clone git@gitlab.com:contextualcode/bash-aliases.git ~/.contextualcode_bash_aliases
    ```
2. Add following lines to your `.bash_profile` file:
    ```
    if [ -f ~/.contextualcode_bash_aliases/all ]; then
        . ~/.contextualcode_bash_aliases/all
    fi
    ```
## Examples
### Platform.sh common
1. Upload local `flcourts` database to current Platform.sh environment:
    ```bash
    platformsh-db-upload flcourts
    ```
2. Upload local `ezpublish_legacy/var` folder to current Platform.sh environment:
    ```bash
    platformsh-mount-upload ezpublish_legacy/var
    ```
3. Deploy current Platform.sh environment and skip `BUSINESS_HOURS` check:
    ```bash
    platformsh-deploy
    ```
4. Force current Platform.sh environment build:
    ```bash
    platformsh-force-build
    ```
5. Add all Contextual Code users as admins for `og2woin2huxcw` Platform.sh project:
    ```bash
    platformsh-add-user-project og2woin2huxcw
    ```
6. Add all Contextual Code users as `master` environment admins for  `og2woin2huxcw` Platform.sh project:
    ```bash
    platformsh-add-user-env og2woin2huxcw master
    ```

### Platform.sh logs
1. Get `20` top used user agents on `2019-01-21` for `flcourts` project:
    ```bash
    platformsh-logs-get-user-agents flcourts 2019-01-21 20
    ```
2. Get `5` most active IPs on `2019-01-21` for `flcourts` project:
    ```bash
    platformsh-logs-get-ips flcourts 2019-01-21 5
    ```
3. Get `10` most active IP masks on `2019-07-30` for `flcourts` project:
    ```bash
    platformsh-logs-get-ip-masks flcourts 2019-07-30 10
    ```
4. Get `20` most active `207.46.13.x` IPs on `2019-07-30` for `flcourts` project:
    ```bash
    platformsh-logs-get-ips-by-mask flcourts 2019-07-30 207.46.13 20
    ```
5. Get `10` top visited pages with execution time more then 2 seconds (2000ms) on `2019-01-21` for `flcourts` project:
    ```bash
    platformsh-logs-get-heavy-pages flcourts 2019-01-21 2000 5
    ```
6. Get PHP-FPM memory usage on `2019-01-21` for `flcourts` project:
    ```bash
    platformsh-logs-get-memory-usage flcourts 2019-01-21
    ```
7. Get average RAM usage for PHP-FPM request for `flcourts` project, during `2019-01-19 - 2019-01-25` timespan:
    ```
    platformsh-logs-get-combined-memory-usage flcourts '2019-01-19,2019-01-20,2019-01-21,2019-01-22,2019-01-23,2019-01-24,2019-01-25'
    ```
8. Get top `20` extensions for all 404 requests on `flcourts` from `2019-01-20` during next `10` days:
    ```
    platformsh-logs-get-404-extensions flcourts 2019-01-20 10 20
    ```
9. Get urls for 404 responses (only `gif, xml, pdf, jpg` extensions) on `flcourts` from `2019-01-20` during next `10` days, which were requested at least `200` times:
    ```
    platformsh-logs-get-404-requests flcourts 2019-01-20 10 'gif,xml,pdf,jpg' 200
    ```
10. Get `20` requests with top `average` RAM usage on `2019-09-18` for `fdacs` project:
    ```
    platformsh-logs-get-top-memory-usage fdacs 2019-09-18 20 avg
    ```
11. Get `10` requests (excluding ESI) with top `total` RAM usage on `2019-09-18` for `fdacs` project:
    ```
    platformsh-logs-get-top-memory-usage fdacs 2019-09-18 10 total '/_fragment?'
    ```

### AWS S3

1. Sync eZ Platform project (alabamacu) S3 assets to a bucket/folder for local development 

   ```bash
    sync-s3-assets s3://cc-alabamacu-storage/alabamacu-platform3 s3://cc-alabamacu-storage/alabamacu-local
   ```
